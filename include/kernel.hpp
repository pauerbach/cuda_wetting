#ifndef KERNEL_H
#define KERNEL_H

#include <cuda_runtime.h>

namespace Wetting {
    void initialize();
    void run();
}

#endif
