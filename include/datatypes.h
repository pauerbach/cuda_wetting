#ifndef __DATATYPES_H__
#define __DATATYPES_H__

#include <Eigen/Dense>

typedef Eigen::Matrix<double, 2, 1> vector;
typedef Eigen::Matrix<double, 2, 2, Eigen::RowMajor> matrix;
struct tensor {
    matrix elem[2];
};

#endif // __DATATYPES_H__
