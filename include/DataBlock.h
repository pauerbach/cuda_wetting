#ifndef SIMULATION_DB_H
#define SIMULATION_DB_H

#include <string>
#include <map>
#include <algorithm>

#include "datatypes.h"
template<typename T>
class DataBlock{

public:

    DataBlock(int isize, int jsize) : isize(isize), jsize(jsize) { }

    inline void finalize(){
        for(auto& field : map){
            std::string key = field.first;

            this->map[key].swap(this->map_old[key]);
        }
    }

    inline T* getLayer(std::string key) {
        return &this->map[key][0];
    }

    inline T* getOldLayer(std::string key) {
        return &this->map_old[key][0];
    }

    void addLayer(const std::string key) {
        map.insert(std::make_pair(key, std::vector<T>(isize*jsize)));
        map_old.insert(std::make_pair(key, std::vector<T>(isize*jsize)));
    }

protected:
    std::map<std::string, std::vector<T>> map;
    std::map<std::string, std::vector<T>> map_old;
    int isize, jsize;

};

typedef DataBlock<double> ScalarBlock;
typedef DataBlock<vector> VectorBlock;
typedef DataBlock<matrix> MatrixBlock;

#endif //SIMULATION_DB_H
