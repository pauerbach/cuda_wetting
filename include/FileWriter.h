#ifndef SIMULATION_FILEWRITER_H
#define SIMULATION_FILEWRITER_H

#include <math.h>
#include <sstream>
#include <string>

// #include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkPointData.h>
#include <vtkSmartPointer.h>
#include <vtkStructuredGrid.h>
#include <vtkXMLStructuredGridWriter.h>

#include "DataBlock.h"
#include "datatypes.h"

class FileWriter {
   public:
    struct Configuration {
        int Nx;
        int Ny;
        double dt;
        double h;
        std::string dataPath;
    };

    // methods
    FileWriter(FileWriter::Configuration config);
    void writeTimestep(ScalarBlock* data, VectorBlock* data_vec, MatrixBlock* data_mat, int iter);
    void addToScalarOutput(std::string);
    void addToVectorOutput(std::string);
    void addToMatrixOutput(std::string);

   private:
    // properties
    std::vector<std::string> writtenVTKsteps;
    std::vector<std::string> writeScalarNames;
    std::vector<std::string> writeVectorNames;
    std::vector<std::string> writeMatrixNames;
    vtkSmartPointer<vtkPoints> points;
    vtkSmartPointer<vtkStructuredGrid> sgrid;
    vtkSmartPointer<vtkXMLStructuredGridWriter> writer;
    int noOfTuples;
    int global_extent[6];

    // methods
    FileWriter::Configuration config;
    void writeVTKFile(ScalarBlock* data, VectorBlock* data_vector, MatrixBlock* data_matrix,
                      std::string timeStepString);
    void writePVD();
};

#endif  // SIMULATION_FILEWRITER_H
