
#ifndef __CalcCALC_H__
#define __CalcCALC_H__

#include <algorithm>
#include <stdexcept>

// CUDA includes
#include "cuda_runtime_api.h"

#include "datatypes.h"

template <typename T>
class Calc {
   public:
    __host__ __device__ Calc(int x, int y, double h)
        : Nx(x), Ny(y), h(h), rcp_h_sq(1 / (h * h)), rcp_2h(1 / (2 * h)), length(Nx * Ny) {}

    __host__ __device__ Calc() : Nx(0), Ny(0), h(0), rcp_h_sq(0), rcp_2h(0), length(0) {}

    int Nx, Ny;
    double h;

   protected:
    __host__ __device__ inline unsigned int pos(int x, int y) {
        unsigned int pos = (y * this->Nx) + x;
        return pos;
    }

    double rcp_h_sq;
    double rcp_2h;
    unsigned long length;
};

/*
 * Class for Domains of scalar values
 */
class ScalarCalc : public Calc<double> {
   public:
    // default constructor
    __host__ __device__ ScalarCalc() : Calc() {}
    __host__ __device__ ScalarCalc(int x, int y, double h) : Calc(x, y, h) {}

    __device__ inline double laPlace(const double* __restrict__ dat, int x, int y) {
        return (dat[pos(x - 1, y)] + dat[pos(x + 1, y)] + dat[pos(x, y - 1)] + dat[pos(x, y + 1)] -
                4 * dat[pos(x, y)]) *
               rcp_h_sq;
    }

    __host__ __device__ inline vector grad(const double* __restrict__ dat, int x, int y) {
        return vector(diff_quot_x(dat, x, y), diff_quot_y(dat, x, y));
    }

    __host__ __device__ inline double diff_quot_x(const double* __restrict__ dat, int x, int y) {
        return (dat[pos(x + 1, y)] - dat[pos(x - 1, y)]) * rcp_2h;
    }

    __host__ __device__ inline double diff_quot_y(const double* __restrict__ dat, int x, int y) {
        return (dat[pos(x, y + 1)] - dat[pos(x, y - 1)]) * rcp_2h;
    }
};

class VectorCalc : public Calc<vector> {
   public:
    // default construtor
    __host__ __device__ VectorCalc() : Calc() {}
    __host__ __device__ VectorCalc(int x, int y, double h) : Calc(x, y, h) {}

    __device__ inline vector laPlace(double** dat, int x, int y) {
        vector tmp;

        for (int i = 0; i < 2; i++) {
            tmp[i] = (__ldg(&dat[i][pos(x - 1, y)]) + __ldg(&dat[i][pos(x + 1, y)]) +
                      __ldg(&dat[i][pos(x, y - 1)]) + __ldg(&dat[i][pos(x, y + 1)]) -
                      4 * __ldg(&dat[i][pos(x, y)])) *
                     rcp_h_sq;
        }

        return tmp;
    }

    __device__ inline matrix grad(double** dat, int x, int y) {
        matrix tmp;

        tmp(0, 0) = (__ldg(&dat[0][pos(x + 1, y)]) - __ldg(&dat[0][pos(x - 1, y)])) * rcp_2h;
        tmp(0, 1) = (__ldg(&dat[1][pos(x + 1, y)]) - __ldg(&dat[1][pos(x - 1, y)])) * rcp_2h;
        tmp(1, 0) = (__ldg(&dat[0][pos(x, y + 1)]) - __ldg(&dat[0][pos(x, y - 1)])) * rcp_2h;
        tmp(1, 1) = (__ldg(&dat[1][pos(x, y + 1)]) - __ldg(&dat[1][pos(x, y - 1)])) * rcp_2h;

        return tmp;
    }

    __host__ __device__ inline vector diff_quot_x(const vector* __restrict__ dat, int x, int y) {
        vector tmp;

#pragma unroll
        for (int i = 0; i < 2; i++) {
            tmp(i) = (dat[pos(x + 1, y)][i] - dat[pos(x - 1, y)][i]) * rcp_2h;
        }

        return tmp;
    }

    __host__ __device__ inline vector diff_quot_y(const vector* __restrict__ dat, int x, int y) {
        vector tmp;

#pragma unroll
        for (int i = 0; i < 2; i++) {
            tmp(i) = (dat[pos(x, y + 1)][i] - dat[pos(x, y - 1)][i]) * rcp_2h;
        }

        return tmp;
    }

    __host__ __device__ inline double divergence(double** dat, int x, int y) {
        double del_x = (dat[0][pos(x + 1, y)] - dat[0][pos(x - 1, y)]) * rcp_2h;
        double del_y = (dat[1][pos(x, y + 1)] - dat[1][pos(x, y - 1)]) * rcp_2h;

        return del_x + del_y;
    }
};

class MatrixCalc : public Calc<matrix> {
   public:
    // default construtor
    __host__ __device__ MatrixCalc() : Calc() {}
    __host__ __device__ MatrixCalc(int x, int y, double h) : Calc(x, y, h) {}

    __device__ inline matrix diff_quot_x(double** dat, int x, int y) {
        matrix tmp;
        int l = pos(x - 1, y);
        int r = pos(x + 1, y);
#pragma unroll
        for (int i = 0; i < 2; i++) {
#pragma unroll
            for (int j = 0; j < 2; j++) {
                tmp(i, j) = (__ldg(&dat[j + 2 * i][r]) - __ldg(&dat[j + 2 * i][l])) * rcp_2h;
            }
        }

        return tmp;
    }

    __device__ inline matrix diff_quot_y(double** dat, int x, int y) {
        matrix tmp;
        int u = pos(x, y + 1);
        int d = pos(x, y - 1);
#pragma unroll
        for (int i = 0; i < 2; i++) {
#pragma unroll
            for (int j = 0; j < 2; j++) {
                tmp(i, j) = (__ldg(&dat[j + 2 * i][u]) - __ldg(&dat[j + 2 * i][d])) * rcp_2h;
            }
        }

        return tmp;
    }

    // 0 = (0,0)
    // 1 = (0,1)
    // 2 = (1,0)
    // 3 = (1,1)
    __device__ inline vector divergence(double** dat, int x, int y) {
        int l = pos(x - 1, y);
        int r = pos(x + 1, y);
        int u = pos(x, y + 1);
        int d = pos(x, y - 1);
        double x_part = (__ldg(&dat[0][r]) - __ldg(&dat[0][l])) * rcp_2h +
                        (__ldg(&dat[1][u]) - __ldg(&dat[1][d])) * rcp_2h;

        double y_part = (__ldg(&dat[2][r]) - __ldg(&dat[2][l])) * rcp_2h +
                        (__ldg(&dat[3][u]) - __ldg(&dat[3][d])) * rcp_2h;

        return {x_part, y_part};
    }

    __device__ inline tensor grad(double** dat, int x, int y) {
        tensor tmp;
        tmp.elem[0] = diff_quot_x(dat, x, y);
        tmp.elem[1] = diff_quot_y(dat, x, y);

        return tmp;
    }

    inline matrix laPlace(int x, int y) {
        throw std::runtime_error("laPlace is not yet implemented for MatrixCalcs");
        return matrix();
    }
    inline void applyDirichletBC() {
        throw std::runtime_error("applyDirichletBC is not yet implemented for MatrixCalcs");
    }
};

#endif  // __CalcCALC_H__
