# Wetting-Cuda
CUDA implementation of the wetting simulation using the phasefield approach

Repo also contains the current *(as of 17.04.20)* version of libEigen from their master branch, because version 3.3.4 which comes with Ubuntu 18.04 is not compatible with a CUDA build.