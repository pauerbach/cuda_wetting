#include "kernel.hpp"

int main(int argc, char** argv) {
    cudaSetDevice(1);
    Wetting::initialize();
    Wetting::run();

    return 0;
}
