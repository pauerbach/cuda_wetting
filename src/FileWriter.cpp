
#include "FileWriter.h"

/**
 * Constructor for class FileWriter
 * @param config configuration object for loading files
 */
FileWriter::FileWriter(FileWriter::Configuration config) {
    this->config = config;
    int Nx = this->config.Nx;
    int Ny = this->config.Ny;
    this->noOfTuples = Nx * Ny;

    // create vtk instances
    this->sgrid = vtkSmartPointer<vtkStructuredGrid>::New();
    this->points = vtkSmartPointer<vtkPoints>::New();
    this->writer = vtkSmartPointer<vtkXMLStructuredGridWriter>::New();

    // set up point data
    for (int j = 0; j < Ny; j++) {
        for (int i = 0; i < Nx; i++) {
            int offset = i + j * Nx;

            double x = i * config.h;
            double y = j * config.h;

            this->points->InsertPoint(offset, x, y, 0.);
        }
    }

    // setup writer
    this->writer->SetCompressorTypeToNone();

    // calculate global extent
    this->global_extent[0] = 0;
    this->global_extent[1] = (Nx - 1) * config.h;
    this->global_extent[2] = 0;
    this->global_extent[3] = (Ny - 1) * config.h;
    this->global_extent[4] = 0;
    this->global_extent[5] = 0;

    // add vtkArrays to our structured grid
    this->sgrid->SetExtent(this->global_extent);
    this->sgrid->SetPoints(this->points);
}

void FileWriter::addToScalarOutput(std::string field) { this->writeScalarNames.push_back(field); }

void FileWriter::addToVectorOutput(std::string field) { this->writeVectorNames.push_back(field); }

void FileWriter::addToMatrixOutput(std::string field) { this->writeMatrixNames.push_back(field); }

/**
 * Handles the write and delegates the content to write to the output function
 * @param DataBlock pointer to data
 * @param iteration iteration to write out
 */
void FileWriter::writeTimestep(ScalarBlock* data, VectorBlock* data_vec, MatrixBlock* data_mat, int iter) {
    // calculate name of current output file
    float time = iter * this->config.dt;
    // std::string fileTime = std::to_string(time);

    std::ostringstream out;
    out.precision(17);
    out << std::fixed << time;
    std::string fileTime = out.str();

    std::replace(fileTime.begin(), fileTime.end(), '.', '_');

    // write vtk file with value for time step
    this->writeVTKFile(data, data_vec, data_mat, fileTime);
}

/**
 * Write file for ParaView post processing
 * @param data pointer to data
 * @param timeStepString timestep as string to print
 */
void FileWriter::writeVTKFile(ScalarBlock* data, VectorBlock* data_vector, MatrixBlock* data_matrix,
                              std::string timeStepString) {
    this->writtenVTKsteps.push_back(timeStepString);

    // generate vts file path
    std::string outpath = this->config.dataPath;
    outpath.append(timeStepString).append(".vts");

    for (const auto field : this->writeScalarNames) {
        auto array = vtkSmartPointer<vtkDoubleArray>::New();
        array->SetNumberOfTuples(this->noOfTuples);
        array->SetNumberOfComponents(1);
        array->SetName(field.c_str());
        this->sgrid->GetPointData()->AddArray(array);

        array->SetArray(data->getLayer(field), this->noOfTuples, 1);
    }

    for (const auto field : this->writeVectorNames) {
        auto array = vtkSmartPointer<vtkDoubleArray>::New();
        array->SetNumberOfTuples(this->noOfTuples);
        array->SetNumberOfComponents(2);
        array->SetName(field.c_str());
        this->sgrid->GetPointData()->AddArray(array);

        array->SetArray(data_vector->getLayer(field)->data(), this->noOfTuples * 2, 1);
    }

    for (const auto field : this->writeMatrixNames) {
        auto array = vtkSmartPointer<vtkDoubleArray>::New();
        array->SetNumberOfTuples(this->noOfTuples);
        array->SetNumberOfComponents(4);
        array->SetName(field.c_str());
        this->sgrid->GetPointData()->AddArray(array);

        // array->SetArray(data_matrix->getLayer(field).getData()->data()->data(), this->noOfTuples*4, 1);
        array->SetArray(data_matrix->getLayer(field)->data(), this->noOfTuples * 4, 1);
    }

    // calculate local extent
    int local_extent[6];
    local_extent[0] = 0;
    local_extent[1] = config.Nx - 1;
    local_extent[2] = 0;
    local_extent[3] = config.Ny - 1;
    local_extent[4] = 0;
    local_extent[5] = 0;

    // configure writer and write
    this->sgrid->SetExtent(local_extent);
    this->writer->SetInputData(sgrid);
    this->writer->SetFileName(outpath.c_str());
    this->writer->SetWriteExtent(local_extent);
    this->writer->Write();

    writePVD();
}

/**
 * Writes the pvd file to run the whole simulation in ParaView
 */
void FileWriter::writePVD() {
    FILE* out;
    out = fopen((this->config.dataPath + "simulation.pvd").c_str(), "w");

    fprintf(out, "<?xml version=\"1.0\"?>\n");
    fprintf(out, "<VTKFile type=\"Collection\" version=\"0.1\">\n");
    fprintf(out, "  <Collection>\n");

    std::string tmp;

    for (auto step : this->writtenVTKsteps) {
        std::string d = "    <DataSet timestep=\"";

        tmp.assign(step);
        std::replace(step.begin(), step.end(), '_', '.');

        d.append(step).append(R"(" part="0" file="./)");
        d.append(tmp.append(".vts")).append("\"/>\n");

        fprintf(out, "%s", d.c_str());
    }
    fprintf(out, "  </Collection>\n");
    fprintf(out, "</VTKFile>");

    fclose(out);
}