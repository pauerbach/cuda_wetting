#include <stdio.h>
#include <Eigen/Dense>
#include <chrono>
#include <iostream>

#include <cuda_profiler_api.h>
#include <vtkVersion.h>

#include "dotenv.h"

#include "DataBlock.h"
#include "FieldCalc.hpp"
#include "FileWriter.h"
#include "kernel.hpp"

static void HandleError(cudaError_t err, const char* file, int line) {
    // CUDA error handeling from the "CUDA by example" book
    if (err != cudaSuccess) {
        printf("%s in %s at line %d\n", cudaGetErrorString(err), file, line);
        exit(EXIT_FAILURE);
    }
}

inline void __cudaCheckError(const char* file, const int line) {
    cudaError err = cudaGetLastError();
    if (cudaSuccess != err) {
        fprintf(stderr, "cudaCheckError() failed at %s:%i : %s\n", file, line, cudaGetErrorString(err));
        exit(-1);
    }

    // More careful checking. However, this will affect performance.
    // Comment away if needed.
    err = cudaDeviceSynchronize();
    if (cudaSuccess != err) {
        fprintf(stderr, "cudaCheckError() with sync failed at %s:%i : %s\n", file, line,
                cudaGetErrorString(err));
        exit(-1);
    }

    return;
}

#define HANDLE_ERROR(err) (HandleError(err, __FILE__, __LINE__))
#define CudaCheckError() __cudaCheckError(__FILE__, __LINE__)

#define SQUARE(x) ((x) * (x))
#define CUBE(x) ((x) * (x) * (x))

namespace Wetting {

auto& env = dotenv::env;

const int BLKX = 64;
const int BLKY = 8;

/**
** Host side constants
**/
#pragma region
std::string outputFile;
double h_dt;
double h_h;
int h_Nx;
int h_Ny;

double y_0;
double x_0;
double r;

double h_epsilon;
double h_D;

double h_rho;
double h_eta;
double h_k;
double h_sigma_12;
double h_sigma_13;
double h_sigma_23;
double h_sigma_1;
double h_sigma_2;
double h_sigma_3;
double h_sigma_T;
double h_S_water;
double h_S_air;
double h_S_solid;
double h_T;
double h_LAMBDA;

// precalculated constants for performance reasons
double h_rcp_sigma_1;
double h_rcp_sigma_2;
double h_rcp_sigma_3;
double h_mu_fac;
double h_rcp_h_sq;
double h_rcp_rho;

int writeIter;
int iter;
#pragma endregion

/**
** Device side constants
**/
#pragma region
__constant__ int Nx;
__constant__ int Ny;
__constant__ double dt;
__constant__ double epsilon;
__constant__ double D;
__constant__ double rho;
__constant__ double eta;
__constant__ double k;
__constant__ double sigma_12;
__constant__ double sigma_23;
__constant__ double sigma_13;
__constant__ double sigma_1;
__constant__ double sigma_2;
__constant__ double sigma_3;
__constant__ double sigma_T;
__constant__ double S_water;
__constant__ double S_air;
__constant__ double S_solid;
__constant__ double T;
__constant__ double LAMBDA;

// precalculated constants for performance reasons
__constant__ double rcp_sigma_1;  // reciprocal of the sigma values
__constant__ double rcp_sigma_2;
__constant__ double rcp_sigma_3;
__constant__ double mu_fac;  // factor in mu calculation 4*sigma_t/epsilon
__constant__ double rcp_h_sq;
__constant__ double rcp_rho;

// FDM Calculators on device side
__device__ ScalarCalc* sc;
__device__ VectorCalc* vc;
__device__ MatrixCalc* mc;
#pragma endregion

ScalarBlock* db;
VectorBlock* db_vec;
MatrixBlock* db_mat;

FileWriter* fw;

void initializeDomains() {
    vector* u_tilde = db_vec->getLayer("u_tilde");
    double* E_0 = db->getLayer("E_0");
    double* E_1 = db->getLayer("E_1");
    double* E_2 = db->getLayer("E_2");
    double* E_3 = db->getLayer("E_3");
    double* E_0_old = db->getOldLayer("E_0");
    double* E_1_old = db->getOldLayer("E_1");
    double* E_2_old = db->getOldLayer("E_2");
    double* E_3_old = db->getOldLayer("E_3");

    double* p = db->getLayer("p");

    for (int x = 0; x < h_Nx; x++) {
        for (int y = 0; y < h_Ny; y++) {
            int I = y * h_Nx + x;

            E_0[I] = E_0_old[I] = 1.;
            E_1[I] = E_1_old[I] = 0.;
            E_2[I] = E_2_old[I] = 0.;
            E_3[I] = E_3_old[I] = 1.;
            p[I] = 0;
        }
    }
}
// Return minimum distance between line segment vw and point p
double minimum_distance_line_segment(vector v, vector w, vector p) {
    const double l2 = SQUARE((w - v).norm());  // i.e. |w-v|^2 -  avoid a sqrt
    // Consider the line extending the segment, parameterized as v + t (w - v).
    // We find projection of point p onto the line.
    // It falls where t = [(p-v) . (w-v)] / |w-v|^2
    // We clamp t from [0,1] to handle points outside the segment vw.
    const double t = std::max(0.0, std::min(1.0, (p - v).dot(w - v) / l2));
    const vector projection = v + t * (w - v);
    return (p - projection).norm();
}

void initializeTwoPhasefields() {
    double* c1 = db->getLayer("c1");
    double* c2 = db->getLayer("c2");
    double* c3 = db->getLayer("c3");
    double* c1_old = db->getOldLayer("c1");
    double* c2_old = db->getOldLayer("c2");
    double* c3_old = db->getOldLayer("c3");

    double x_1 = x_0 - r;  // left point of the half circle
    double y_1 = y_0;
    double x_2 = x_0 + r;  // right point of the half circle
    double y_2 = y_0;

    for (int x = 0; x < h_Nx; x++) {
        for (int y = 0; y < h_Ny; y++) {
            int I = y * h_Nx + x;

            c1_old[I] = c1[I] = (std::tanh((y_0 - y * h_h) * 2 / h_epsilon) + 1) / 2;

            double sign = y * h_h > y_1 ? 1 : -1;

            double d = std::min(
                (r - std::sqrt(SQUARE(x * h_h - x_0) + SQUARE(y * h_h - y_0))),  // circle shape for droplet
                minimum_distance_line_segment({x_1, y_1}, {x_2, y_2}, {x * h_h, y * h_h}) * sign);

            c2_old[I] = c2[I] = (std::tanh(d * 2 / h_epsilon) + 1) / 2;

            c3_old[I] = c3[I] = 1 - (c1[I] + c2[I]);
        }
    }
}

__global__ void cu_get_velocity(const double* __restrict__ p, double** __restrict__ u_tilde,
                                double** __restrict__ u_new) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    int I = y * Nx + x;

    if (x < Nx - 1 && x > 0 && y < Ny - 1 && y > 0) {
        vector u_new_tmp = -sc->grad(p, x, y) * rcp_rho * dt;

        for (int i = 0; i < 2; i++) u_new[i][I] = u_new_tmp[i] + u_tilde[i][I];
    }
}

__global__ void cu_get_pressure(const double* __restrict__ p_old, double** __restrict__ u_tilde,
                                double* __restrict__ p_new) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    int I = y * Nx + x;

    if (x < Nx - 1 && x > 0 && y < Ny - 1 && y > 0) {
        double p1 = sc->laPlace(p_old, x, y) / (rho * k);
        double p2 = vc->divergence(u_tilde, x, y) / (k * dt);

        p_new[I] = (p1 - p2) * dt + p_old[I];
    }
}

__global__ void __launch_bounds__(512)
    cu_get_tmp_vel(double** __restrict__ u, const double* __restrict__ c1, const double* __restrict__ c2,
                   const double* __restrict__ c3, const double* __restrict__ mu1,
                   const double* __restrict__ mu2, const double* __restrict__ mu3, double** __restrict__ f,
                   double** __restrict__ u_tilde_new) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    int I = y * Nx + x;

    if (x < Nx - 1 && x > 0 && y < Ny - 1 && y > 0) {
        vector surfaceTension =
            mu1[I] * sc->grad(c1, x, y) + mu2[I] * sc->grad(c2, x, y) + mu3[I] * sc->grad(c3, x, y);
        vector elasticity = mc->divergence(f, x, y);

        vector u_tilde_new_tmp = ((eta * vc->laPlace(u, x, y) + surfaceTension + elasticity) * rcp_rho) * dt;

        for (int i = 0; i < 2; i++) u_tilde_new[i][I] = u_tilde_new_tmp[i] + u[i][I];
    }
}

__global__ void cu_get_f(double** E, const double* c1, const double* c2, double** f) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    int I = y * Nx + x;

    auto heav = [](double x) {
        const double kh = 60;
        return 1 / (1 + __expf(-kh * x));
    };

    if (x < Nx && x >= 0 && y < Ny && y >= 0) {
        double S = (S_solid - S_air) * heav(c1[I] - 0.5) + (S_water - S_air) * heav(c2[I] - 0.5) + S_air;

        matrix _E;
        _E << E[0][I], E[1][I], E[2][I], E[3][I];
        matrix tmp_f = S * (_E - matrix::Identity());
        f[0][I] = tmp_f(0, 0);
        f[1][I] = tmp_f(0, 1);
        f[2][I] = tmp_f(1, 0);
        f[3][I] = tmp_f(1, 1);
    }
}

__global__ void cu_get_phasefield(const double* __restrict__ c1, const double* __restrict__ c2,
                                  const double* __restrict__ c3, double** __restrict__ u,
                                  const double* __restrict__ mu1, const double* __restrict__ mu2,
                                  const double* __restrict__ mu3, double* __restrict__ c1_new,
                                  double* __restrict__ c2_new, double* __restrict__ c3_new) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    int I = y * Nx + x;

    if (x < Nx - 1 && x > 0 && y < Ny - 1 && y > 0) {
        double vel1 = vector(u[0][I], u[1][I]).dot(sc->grad(c1, x, y));
        double lapla1 = (D * rcp_sigma_1) * sc->laPlace(mu1, x, y);
        c1_new[I] = (lapla1 - vel1) * dt + c1[I];

        double vel2 = vector(u[0][I], u[1][I]).dot(sc->grad(c2, x, y));
        double lapla2 = (D * rcp_sigma_2) * sc->laPlace(mu2, x, y);
        c2_new[I] = (lapla2 - vel2) * dt + c2[I];

        c3_new[I] = 1 - (c1_new[I] + c2_new[I]);
    }
}

__global__ void cu_get_mu(const double* __restrict__ c1, const double* __restrict__ c2, const double* __restrict__ c3,
                          double* __restrict__ mu1, double* __restrict__ mu2, double* __restrict__ mu3) {
    int x = (blockIdx.x * blockDim.x + threadIdx.x);
    int y = (blockIdx.y * blockDim.y + threadIdx.y);

    if (x < Nx - 1 && x > 0 && y < Ny - 1 && y > 0) {
        int I = y * Nx + x;

        double F_c1 = 2 * sigma_12 * c1[I] * SQUARE(c2[I]) + 2 * sigma_13 * c1[I] * SQUARE(c3[I]) +
                      2 * sigma_1 * c1[I] * c2[I] * c3[I] + sigma_2 * SQUARE(c2[I]) * c3[I] +
                      sigma_3 * c2[I] * SQUARE(c3[I]) + LAMBDA * 2 * c1[I] * SQUARE(c2[I]) * SQUARE(c3[I]);

        double F_c2 = 2 * sigma_12 * SQUARE(c1[I]) * c2[I] + 2 * sigma_23 * c2[I] * SQUARE(c3[I]) +
                      sigma_1 * SQUARE(c1[I]) * c3[I] + 2 * sigma_2 * c1[I] * c2[I] * c3[I] +
                      sigma_3 * c1[I] * SQUARE(c3[I]) + LAMBDA * 2 * SQUARE(c1[I]) * c2[I] * SQUARE(c3[I]);

        double F_c3 = 2 * sigma_13 * SQUARE(c1[I]) * c3[I] + 2 * sigma_23 * SQUARE(c2[I]) * c3[I] +
                      sigma_1 * SQUARE(c1[I]) * c2[I] + sigma_2 * c1[I] * SQUARE(c2[I]) +
                      2 * sigma_3 * c1[I] * c2[I] * c3[I] +
                      LAMBDA * 2 * SQUARE(c1[I]) * SQUARE(c2[I]) * c3[I];

        mu1[I] = mu_fac * ((rcp_sigma_2 * (F_c1 - F_c2)) + (rcp_sigma_3 * (F_c1 - F_c3))) -
                 0.75f * epsilon * sigma_1 * sc->laPlace(c1, x, y);

        mu2[I] = mu_fac * (rcp_sigma_1 * (F_c2 - F_c1) + rcp_sigma_3 * (F_c2 - F_c3)) -
                 0.75f * epsilon * sigma_2 * sc->laPlace(c2, x, y);

        mu3[I] = mu_fac * (rcp_sigma_1 * (F_c3 - F_c1) + rcp_sigma_2 * (F_c3 - F_c2)) -
                 0.75f * epsilon * sigma_3 * sc->laPlace(c3, x, y);
    }
}

__global__ void cu_get_E(double** __restrict__ E, double** __restrict__ u,
                         const double* __restrict__ c1, const double* __restrict__ c2, const double* __restrict__ c3,
                         double** __restrict__ E_new) {
    int x = (blockIdx.x * blockDim.x + threadIdx.x);
    int y = (blockIdx.y * blockDim.y + threadIdx.y);

    int I = y * Nx + x;

    if (x < Nx - 1 && x > 0 && y < Ny - 1 && y > 0) {
        matrix _E;
        _E << E[0][I], E[1][I], E[2][I], E[3][I];

        matrix transp = vc->grad(u, x, y).transpose() * _E;
        matrix nontransp = _E * vc->grad(u, x, y);

        double c1_tmp = c1[I] < 0.05 ? 0.0 : c1[I];
        double c2_tmp = c2[I] < 0.05 ? 0.0 : c2[I];
        double c3_tmp = c3[I] < 0.05 ? 0.0 : c3[I];

        double alpha = c2_tmp + c3_tmp;
        double lambda = fmax(c1_tmp * T, 1e-8);
        matrix restriction = alpha * (_E - matrix::Identity()) / lambda;

        // v·∇σ
        auto gradE = mc->grad(E, x, y);
        matrix convection = u[0][I] * gradE.elem[0] + u[1][I] * gradE.elem[1];

        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                E_new[j + 2 * i][I] =
                    (transp(i, j) + nontransp(i, j) - restriction(i, j) - convection(i, j)) * dt +
                    E[j + 2 * i][I];
    }
}

__device__ void swap_one(double* a, double* b) {
    double* tmp = a;
    a = b;
    b = tmp;
}

__global__ void cu_swap(double** E, double** E_old, double* p, double* p_old, double** u, double** u_old,
                        double* c1, double* c1_old, double* c2, double* c2_old, double* c3, double* c3_old) {
    swap_one(E_old[0], E[0]);
    swap_one(E_old[1], E[1]);
    swap_one(E_old[2], E[2]);
    swap_one(E_old[3], E[3]);
    swap_one(p_old, p);
    swap_one(u_old[0], u[0]);
    swap_one(u_old[1], u[1]);
    swap_one(c1_old, c1);
    swap_one(c2_old, c2);
    swap_one(c3_old, c3);
}

template <class T>
void host_swap(T*& a, T*& b) {
    T* temp = a;
    a = b;
    b = temp;
}

__global__ void cu_applyDirichletBC(double** a) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    int I = y * Nx + x;

    if ((x == 0 || x == 1) && y < Ny - 1) a[0][I] = a[1][I] = 0.;
    if ((x == Nx - 1 || x == Nx - 2) && y < Ny - 1) a[0][I] = a[1][I] = 0.;
    if ((y == 0 || y == 1) && x < Nx - 1) a[0][I] = a[1][I] = 0.;
    if ((y == Ny - 1 || y == Ny - 2) && x < Nx - 1) a[0][I] = a[1][I] = 0.;

    return;
}

__global__ void cu_applyBC(double* a) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    int I = y * Nx + x;
    int l = y * Nx + (x - 1);
    int r = y * Nx + (x + 1);
    int u = (y + 1) * Nx + x;
    int d = (y - 1) * Nx + x;

    if (x == 0 && y < Ny - 1) a[I] = a[r];
    if (x == Nx - 1 && y < Ny - 1) a[I] = a[l];
    if (y == 0 && x < Nx - 1) a[I] = a[u];
    if (y == Ny - 1 && x < Nx - 1) a[I] = a[d];

    return;
}

__global__ void cu_applyBCVector(double** a) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    int I = y * Nx + x;

    int l = y * Nx + (x - 1);
    int r = y * Nx + (x + 1);
    int u = (y + 1) * Nx + x;
    int d = (y - 1) * Nx + x;

    if (x == 0 && y < Ny - 1) {
        a[0][I] = a[0][r];
        a[1][I] = a[1][r];
    }
    if (x == Nx - 1 && y < Ny - 1) {
        a[0][I] = a[0][l];
        a[1][I] = a[1][l];
    }
    if (y == 0 && x < Nx - 1) {
        a[0][I] = a[0][u];
        a[1][I] = a[1][u];
    }
    if (y == Ny - 1 && x < Nx - 1) {
        a[0][I] = a[0][d];
        a[1][I] = a[1][d];
    }

    return;
}

__global__ void cu_applyBCMatrix(double** a) {
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;

    int I = y * Nx + x;

    int l = y * Nx + (x - 1);
    int r = y * Nx + (x + 1);
    int u = (y + 1) * Nx + x;
    int d = (y - 1) * Nx + x;

    if (x == 0 && y < Ny - 1) {
        a[0][I] = a[0][r];
        a[1][I] = a[1][r];
        a[2][I] = a[2][r];
        a[3][I] = a[3][r];
    }
    if (x == Nx - 1 && y < Ny - 1) {
        a[0][I] = a[0][l];
        a[1][I] = a[1][l];
        a[2][I] = a[2][l];
        a[3][I] = a[3][l];
    }
    if (y == 0 && x < Nx - 1) {
        a[0][I] = a[0][u];
        a[1][I] = a[1][u];
        a[2][I] = a[2][u];
        a[3][I] = a[3][u];
    }
    if (y == Ny - 1 && x < Nx - 1) {
        a[0][I] = a[0][d];
        a[1][I] = a[1][d];
        a[2][I] = a[2][d];
        a[3][I] = a[3][d];
    }

    return;
}

template <class T>
T* __create_device_scalar(T* data) {
    T* s_data;
    HANDLE_ERROR(cudaMalloc((void**)&s_data, sizeof(T) * h_Nx * h_Ny));
    HANDLE_ERROR(cudaMemcpy(s_data, data, sizeof(T) * h_Nx * h_Ny, cudaMemcpyHostToDevice));

    return s_data;
}
double* create_device_scalar(double* host_data) { return __create_device_scalar<double>(host_data); }

double** create_device_vector(double* a_0, double* a_1){
    double** dev_A;
    HANDLE_ERROR(cudaMalloc(&dev_A, sizeof(double*) * 2));
    HANDLE_ERROR(cudaMemcpy(dev_A, &a_0, sizeof(double*), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(dev_A + 1, &a_1, sizeof(double*), cudaMemcpyHostToDevice));
    
    return dev_A;
}
double** create_device_matrix(double* a_0, double* a_1, double* a_2, double* a_3){
    double** dev_A;
    HANDLE_ERROR(cudaMalloc(&dev_A, sizeof(double*) * 4));
    HANDLE_ERROR(cudaMemcpy(dev_A, &a_0, sizeof(double*), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(dev_A + 1, &a_1, sizeof(double*), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(dev_A + 2, &a_2, sizeof(double*), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(dev_A + 3, &a_3, sizeof(double*), cudaMemcpyHostToDevice));
    
    return dev_A;
}

void initialize() {
    outputFile = env["OUTPUTFILE"];
    h_dt = stof(env["DT"]);
    h_h = stof(env["H"]);
    h_Nx = stof(env["NX"]);
    h_Ny = stof(env["NY"]);

    y_0 = stof(env["Y0"]);
    x_0 = stof(env["X0"]);
    r = stof(env["R"]);

    h_epsilon = stof(env["EPSILON"]);
    h_D = stof(env["D"]);

    h_rho = stof(env["RHO"]);
    h_eta = stof(env["ETA"]);
    h_k = 4 * h_dt / (SQUARE(h_h) * h_rho);
    h_sigma_12 = stof(env["SIGMA_12"]);
    h_sigma_13 = stof(env["SIGMA_13"]);
    h_sigma_23 = stof(env["SIGMA_23"]);
    h_sigma_1 = (h_sigma_12 - h_sigma_23 + h_sigma_13);
    h_sigma_2 = (h_sigma_12 + h_sigma_23 - h_sigma_13);
    h_sigma_3 = (-h_sigma_12 + h_sigma_23 + h_sigma_13);
    h_sigma_T = 3. / (1. / h_sigma_1 + 1. / h_sigma_2 + 1. / h_sigma_3);
    h_S_water = stof(env["S_WATER"]);
    h_S_air = stof(env["S_AIR"]);
    h_S_solid = stof(env["S_SOLID"]);
    h_T = stof(env["T"]);
    h_LAMBDA = stof(env["LAMBDA"]);

    // precalculated constants for performance reasons
    h_rcp_sigma_1 = 1 / h_sigma_1;
    h_rcp_sigma_2 = 1 / h_sigma_2;
    h_rcp_sigma_3 = 1 / h_sigma_3;
    h_mu_fac = 4 * h_sigma_T / h_epsilon;
    h_rcp_h_sq = 1 / (h_h * h_h);
    h_rcp_rho = 1 / h_rho;

    writeIter = stof(env["WRITEITER"]);
    iter = stof(env["END_TIME"]) / h_dt + writeIter;
    // writeIter = 500;
    // iter = 10000;
    // iter = 5;

    // set parameters on device memory
    cudaMemcpyToSymbol(Nx, &h_Nx, sizeof(int), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(Ny, &h_Ny, sizeof(int), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(dt, &h_dt, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(epsilon, &h_epsilon, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(D, &h_D, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(rho, &h_rho, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(eta, &h_eta, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(k, &h_k, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(sigma_12, &h_sigma_12, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(sigma_23, &h_sigma_23, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(sigma_13, &h_sigma_13, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(sigma_T, &h_sigma_T, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(sigma_1, &h_sigma_1, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(sigma_2, &h_sigma_2, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(sigma_3, &h_sigma_3, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(rcp_sigma_1, &h_rcp_sigma_1, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(rcp_sigma_2, &h_rcp_sigma_2, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(rcp_sigma_3, &h_rcp_sigma_3, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(mu_fac, &h_mu_fac, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(S_water, &h_S_water, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(S_air, &h_S_air, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(S_solid, &h_S_solid, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(T, &h_T, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(LAMBDA, &h_LAMBDA, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(rcp_h_sq, &h_rcp_h_sq, sizeof(double), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(rcp_rho, &h_rcp_rho, sizeof(double), 0, cudaMemcpyHostToDevice);

    /*
    ** Create Calculator instances
    */
    ScalarCalc h_sc(h_Nx, h_Ny, h_h);
    VectorCalc h_vc(h_Nx, h_Ny, h_h);
    MatrixCalc h_mc(h_Nx, h_Ny, h_h);

    ScalarCalc* d_sc;
    ScalarCalc* d_vc;
    ScalarCalc* d_mc;
    HANDLE_ERROR(cudaMalloc(&d_sc, sizeof(ScalarCalc)));
    HANDLE_ERROR(cudaMalloc(&d_vc, sizeof(VectorCalc)));
    HANDLE_ERROR(cudaMalloc(&d_mc, sizeof(MatrixCalc)));
    HANDLE_ERROR(cudaMemcpy(d_sc, &h_sc, sizeof(ScalarCalc), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(d_vc, &h_vc, sizeof(VectorCalc), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(d_mc, &h_mc, sizeof(MatrixCalc), cudaMemcpyHostToDevice));

    cudaMemcpyToSymbol(sc, &d_sc, sizeof(ScalarCalc*), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(vc, &d_vc, sizeof(ScalarCalc*), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(mc, &d_mc, sizeof(ScalarCalc*), 0, cudaMemcpyHostToDevice);

    db = new ScalarBlock(h_Nx, h_Ny);

    db->addLayer("p");
    db->addLayer("mu1");
    db->addLayer("mu2");
    db->addLayer("mu3");
    db->addLayer("c1");
    db->addLayer("c2");
    db->addLayer("c3");

    db->addLayer("f_0");
    db->addLayer("f_1");
    db->addLayer("f_2");
    db->addLayer("f_3");

    db->addLayer("E_0");
    db->addLayer("E_1");
    db->addLayer("E_2");
    db->addLayer("E_3");

    db->addLayer("u_0");
    db->addLayer("u_1");

    db->addLayer("u_tilde_0");
    db->addLayer("u_tilde_1");

    // vector fields
    db_vec = new VectorBlock(h_Nx, h_Ny);

    // matrix fields
    db_mat = new MatrixBlock(h_Nx, h_Ny);

    FileWriter::Configuration fwConf;
    fwConf.dataPath = outputFile;
    fwConf.dt = h_dt;
    fwConf.Nx = h_Nx;
    fwConf.Ny = h_Ny;
    fwConf.h = h_h;
    fw = new FileWriter(fwConf);

    fw->addToScalarOutput("p");

    fw->addToScalarOutput("c1");
    fw->addToScalarOutput("c2");
    fw->addToScalarOutput("c3");
    fw->addToScalarOutput("mu1");
    fw->addToScalarOutput("mu2");
    fw->addToScalarOutput("mu3");

    fw->addToScalarOutput("f_0");
    fw->addToScalarOutput("f_1");
    fw->addToScalarOutput("f_2");
    fw->addToScalarOutput("f_3");

    fw->addToScalarOutput("E_0");
    fw->addToScalarOutput("E_1");
    fw->addToScalarOutput("E_2");
    fw->addToScalarOutput("E_3");

    fw->addToScalarOutput("u_0");
    fw->addToScalarOutput("u_1");

    fw->addToScalarOutput("u_tilde_0");
    fw->addToScalarOutput("u_tilde_1");

    initializeDomains();
    initializeTwoPhasefields();

    fw->writeTimestep(db, db_vec, db_mat, 0);

    db->finalize();
    db_vec->finalize();
    db_mat->finalize();

    printf("Eigen version: %d.%d.%d\n", EIGEN_WORLD_VERSION, EIGEN_MAJOR_VERSION, EIGEN_MINOR_VERSION);
    std::cout << vtkVersion::GetVTKSourceVersion() << std::endl;
}

void run() {
    double* c1_old = db->getOldLayer("c1");
    double* c2_old = db->getOldLayer("c2");
    double* c3_old = db->getOldLayer("c3");
    double* c1 = db->getLayer("c1");
    double* c2 = db->getLayer("c2");
    double* c3 = db->getLayer("c3");

    double* mu1 = db->getLayer("mu1");
    double* mu2 = db->getLayer("mu2");
    double* mu3 = db->getLayer("mu3");

    double* f_0 = db->getLayer("f_0");
    double* f_1 = db->getLayer("f_1");
    double* f_2 = db->getLayer("f_2");
    double* f_3 = db->getLayer("f_3");

    double* E_0 = db->getLayer("E_0");
    double* E_1 = db->getLayer("E_1");
    double* E_2 = db->getLayer("E_2");
    double* E_3 = db->getLayer("E_3");
    double* E_0_old = db->getOldLayer("E_0");
    double* E_1_old = db->getOldLayer("E_1");
    double* E_2_old = db->getOldLayer("E_2");
    double* E_3_old = db->getOldLayer("E_3");

    double* p = db->getLayer("p");
    double* p_old = db->getOldLayer("p");

    double* u_0 = db->getLayer("u_0");
    double* u_1 = db->getLayer("u_1");
    double* u_0_old = db->getOldLayer("u_0");
    double* u_1_old = db->getOldLayer("u_1");

    double* u_tilde_0 = db->getLayer("u_tilde_0");
    double* u_tilde_1 = db->getLayer("u_tilde_1");

    double* dev_c1_old = create_device_scalar(c1_old);
    double* dev_c2_old = create_device_scalar(c2_old);
    double* dev_c3_old = create_device_scalar(c3_old);
    double* dev_c1 = create_device_scalar(c1);
    double* dev_c2 = create_device_scalar(c2);
    double* dev_c3 = create_device_scalar(c3);

    double* dev_mu1 = create_device_scalar(mu1);
    double* dev_mu2 = create_device_scalar(mu2);
    double* dev_mu3 = create_device_scalar(mu3);

    double* dev_f_0 = create_device_scalar(f_0);
    double* dev_f_1 = create_device_scalar(f_1);
    double* dev_f_2 = create_device_scalar(f_2);
    double* dev_f_3 = create_device_scalar(f_3);

    double* dev_E_0 = create_device_scalar(E_0);
    double* dev_E_1 = create_device_scalar(E_1);
    double* dev_E_2 = create_device_scalar(E_2);
    double* dev_E_3 = create_device_scalar(E_3);

    double* dev_E_0_old = create_device_scalar(E_0_old);
    double* dev_E_1_old = create_device_scalar(E_1_old);
    double* dev_E_2_old = create_device_scalar(E_2_old);
    double* dev_E_3_old = create_device_scalar(E_3_old);

    double* dev_u_0 = create_device_scalar(u_0);
    double* dev_u_1 = create_device_scalar(u_1);
    double* dev_u_0_old = create_device_scalar(u_0_old);
    double* dev_u_1_old = create_device_scalar(u_1_old);

    double* dev_u_tilde_0 = create_device_scalar(u_tilde_0);
    double* dev_u_tilde_1 = create_device_scalar(u_tilde_1);

    double** dev_f = create_device_matrix(dev_f_0, dev_f_1, dev_f_2, dev_f_3);

    double** dev_E = create_device_matrix(dev_E_0, dev_E_1, dev_E_2, dev_E_3);
    double** dev_E_old = create_device_matrix(dev_E_0_old, dev_E_1_old, dev_E_2_old, dev_E_3_old);

    double** dev_u = create_device_vector(dev_u_0, dev_u_1);
    double** dev_u_old = create_device_vector(dev_u_0_old, dev_u_1_old);

    double** dev_u_tilde = create_device_vector(dev_u_tilde_0, dev_u_tilde_1);

    double* dev_p_old = create_device_scalar(p_old);
    double* dev_p = create_device_scalar(p);

    dim3 dimBlock((int)std::ceil(h_Nx / (double)BLKX), (int)std::ceil(h_Ny / (double)BLKY));
    dim3 dimGrid(BLKX, BLKY);

    cudaProfilerStart();
    auto start = std::chrono::steady_clock::now();
    for (int i = 1; i < iter; i++) {
        cu_get_E<<<dimBlock, dimGrid>>>(dev_E_old, dev_u_old, dev_c1_old, dev_c2_old, dev_c3_old, dev_E);
        cu_get_mu<<<dimBlock, dimGrid>>>(dev_c1_old, dev_c2_old, dev_c3_old, dev_mu1, dev_mu2, dev_mu3);

        cu_applyBCMatrix<<<dimBlock, dimGrid>>>(dev_E);
        cu_applyBC<<<dimBlock, dimGrid>>>(dev_mu1);
        cu_applyBC<<<dimBlock, dimGrid>>>(dev_mu2);
        cu_applyBC<<<dimBlock, dimGrid>>>(dev_mu3);

        cu_get_f<<<dimBlock, dimGrid>>>(dev_E, dev_c1_old, dev_c2_old, dev_f);

        cu_get_tmp_vel<<<dimBlock, dimGrid>>>(dev_u_old, dev_c1_old, dev_c2_old, dev_c3_old, dev_mu1, dev_mu2,
                                              dev_mu3, dev_f, dev_u_tilde);
        cu_applyBCVector<<<dimBlock, dimGrid>>>(dev_u_tilde);

        cu_get_pressure<<<dimBlock, dimGrid>>>(dev_p_old, dev_u_tilde, dev_p);
        cu_applyBC<<<dimBlock, dimGrid>>>(dev_p);

        cu_get_velocity<<<dimBlock, dimGrid>>>(dev_p, dev_u_tilde, dev_u);
        cu_applyDirichletBC<<<dimBlock, dimGrid>>>(dev_u);

        cu_get_phasefield<<<dimBlock, dimGrid>>>(dev_c1_old, dev_c2_old, dev_c3_old, dev_u, dev_mu1, dev_mu2,
                                                 dev_mu3, dev_c1, dev_c2, dev_c3);
        cu_applyBC<<<dimBlock, dimGrid>>>(dev_c1);
        cu_applyBC<<<dimBlock, dimGrid>>>(dev_c2);
        cu_applyBC<<<dimBlock, dimGrid>>>(dev_c3);

        cu_swap<<<1, 1>>>(dev_E_old, dev_E,
                          dev_p, dev_p_old,
                          dev_u_old, dev_u,
                          dev_c1_old, dev_c1,
                          dev_c2_old, dev_c2,
                          dev_c3_old, dev_c3);

        host_swap<double*>(dev_E_old, dev_E);
        host_swap<double>(dev_E_0_old, dev_E_0);
        host_swap<double>(dev_E_1_old, dev_E_1);
        host_swap<double>(dev_E_2_old, dev_E_2);
        host_swap<double>(dev_E_3_old, dev_E_3);

        host_swap<double>(dev_p, dev_p_old);

        host_swap<double*>(dev_u, dev_u_old);
        host_swap<double>(dev_u_0, dev_u_0_old);
        host_swap<double>(dev_u_1, dev_u_1_old);

        host_swap<double>(dev_c1_old, dev_c1);
        host_swap<double>(dev_c2_old, dev_c2);
        host_swap<double>(dev_c3_old, dev_c3);

        if (i % writeIter == 0 || i < 10) {
            auto end = std::chrono::steady_clock::now();
            long long duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
            printf("Iter: %d (%.4f ms)\n", i, (float)duration / writeIter);

            HANDLE_ERROR(cudaMemcpy(E_0, dev_E_0_old, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));
            HANDLE_ERROR(cudaMemcpy(E_1, dev_E_1_old, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));
            HANDLE_ERROR(cudaMemcpy(E_2, dev_E_2_old, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));
            HANDLE_ERROR(cudaMemcpy(E_3, dev_E_3_old, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));

            HANDLE_ERROR(cudaMemcpy(u_0, dev_u_0_old, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));
            HANDLE_ERROR(cudaMemcpy(u_1, dev_u_1_old, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));

            HANDLE_ERROR(cudaMemcpy(p, dev_p_old, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));

            HANDLE_ERROR(cudaMemcpy(mu1, dev_mu1, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));
            HANDLE_ERROR(cudaMemcpy(mu2, dev_mu2, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));
            HANDLE_ERROR(cudaMemcpy(mu3, dev_mu3, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));

            HANDLE_ERROR(cudaMemcpy(c1, dev_c1_old, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));
            HANDLE_ERROR(cudaMemcpy(c2, dev_c2_old, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));
            HANDLE_ERROR(cudaMemcpy(c3, dev_c3_old, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));

            HANDLE_ERROR(cudaMemcpy(f_0, dev_f_0, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));
            HANDLE_ERROR(cudaMemcpy(f_1, dev_f_1, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));
            HANDLE_ERROR(cudaMemcpy(f_2, dev_f_2, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));
            HANDLE_ERROR(cudaMemcpy(f_3, dev_f_3, sizeof(double) * h_Nx * h_Ny, cudaMemcpyDeviceToHost));

            fw->writeTimestep(db, db_vec, db_mat, i);
            start = std::chrono::steady_clock::now();
        }
    }
    cudaProfilerStop();
}

}  // namespace Wetting
